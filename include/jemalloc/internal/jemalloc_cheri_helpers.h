#ifndef JEMALLOC_CHERI_HELPERS_H
#define JEMALLOC_CHERI_HELPERS_H

#ifdef __CHERI_PURE_CAPABILITY__
#include <sys/cheri.h>

JEMALLOC_ALWAYS_INLINE size_t
size_to_representable_length(size_t size, int *cap_flags) {
	size_t cap_size = cheri_representable_length(size);
	if (cap_size < size) {
		/* Never decrease the size */
		return SIZE_MAX;
	} else {
		if (size < cap_size && cap_flags != NULL) {
			*cap_flags |= CAP_LENGTH_ADJUSTED;
		}
		return cap_size;
	}
}

JEMALLOC_ALWAYS_INLINE size_t
size_to_representable_alignment(size_t size, size_t alignment,
    int *cap_flags) {
	size_t cap_alignment = ~cheri_representable_alignment_mask(size) + 1;
	if (cap_alignment <= alignment) {
		/* Never decrease the alignment */
		return alignment;
	} else {
		/*
		 * An increase from a minimum alignment of 0 to a
		 * newly-obtained value of 1 does not represent a change in the
		 * alignment requirement, and since some of the paths perform
		 * an explicit comparison with 0 in order to reduce overhead,
		 * the original alignment can be returned for those cases.
		 */
		if ((cap_alignment == 1) && (alignment == 0)) {
			return 0;
		}
		if (cap_flags != NULL) {
			*cap_flags |= CAP_ALIGNMENT_ADJUSTED;
		}
		return cap_alignment;
	}
}

JEMALLOC_ALWAYS_INLINE bool
is_cap_aligned(void *ptr, size_t alignment) {
	return ((alignment == 0) || (__builtin_is_aligned(ptr, alignment)));
}

JEMALLOC_ALWAYS_INLINE void *
extent_derive_bounds(void *ptr, extent_t *extent, bool restrict_bounds,
    size_t length, bool change_address) {
	/* Derive new bounds using the addr extent_t entry. */
	void *ret = extent_addr_get(extent);
	assert(ptr != NULL);
	ptraddr_t new_address = (ptraddr_t)(uintptr_t)ptr;
	if (change_address) {
		ret = cheri_address_set(ret, new_address);
	} else {
		assert(new_address == (ptraddr_t)(uintptr_t)ret);
	}
	if (restrict_bounds) {
		ret = cheri_bounds_set_exact(ret, length);
		ret = cheri_perms_clear(ret, CHERI_PERM_SW_VMEM);
	}
	return ret;
}
#endif

#endif
